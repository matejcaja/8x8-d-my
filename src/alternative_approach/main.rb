require_relative("board_impl")

ALPHA = 0.95
PROGRAM_ITERATIONS = 100
MAX_ITERATIONS = 5000
TEMP = 100.0

for i in (0...PROGRAM_ITERATIONS)

  temp = TEMP
  board_impl = Board_Impl.new
  queens = board_impl.initialize_queens
  attacks = board_impl.calculate_attacks(queens)
  final_iter = 0

  for i in (0...MAX_ITERATIONS)
    final_iter = i

      temp *= ALPHA
      new_queens = board_impl.propose_solution(queens)
      new_attacks = board_impl.calculate_attacks(new_queens)

      if board_impl.acceptance_probability(new_attacks,attacks,temp)
        queens = board_impl.accept_new_solution(new_queens)
        attacks = new_attacks
        #puts "#{attacks}"
      end

      if attacks == 0
        final_iter = i
        break
      end
  end

  board_impl.print_board(queens)
  board_impl.calculate_attacks(queens)
  puts attacks
  puts final_iter
end
















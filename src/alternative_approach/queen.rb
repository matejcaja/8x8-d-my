class Queen
  attr_accessor :x, :y

  def initialize(x, y)
    self.x = x
    self.y = y
  end

  def copy_yourself
    Queen.new(self.x, self.y)
  end
end
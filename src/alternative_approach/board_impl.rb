require_relative("queen")

#define constants
SIZE = 8

class Board_Impl
  def print_board(queens)
    board = Array.new(SIZE) {Array.new(SIZE)}

    queens.each do |queen|
      board[queen.x][queen.y] = 1
    end

    (0...SIZE).each do |i|
      puts "\n" if i > 0
      (0...SIZE).each do |j|
        board[i][j].nil? ? print('0 ') : print(board[i][j].to_s + ' ')
      end
    end
    puts "\n\n"
  end

  def initialize_queens
    queens = Array.new
    board = Array.new(SIZE) {Array.new(SIZE)}
    processed = 0

    until processed == SIZE
      x = Random.rand(0...SIZE)
      y = Random.rand(0...SIZE)

      unless board[x][y] == 1
        board[x][y] = 1
        queens << Queen.new(x,y)
        processed += 1
      end
    end
    queens
  end

  def calculate_attacks(queens)
    attacks = 0

    queens.each do |queen|
      for i in 0...SIZE
        queen_to_check = queens[i]
        unless queen == queen_to_check #omit yourself
          attacks += 1 if queen.x == queen_to_check.x #check rows for queens
          attacks += 1 if queen.y == queen_to_check.y #check columns for queens
          attacks += 1 if ((queen.y - queen.x) == (queen_to_check.y - queen_to_check.x)) #check left diagonale
          attacks += 1 if ((queen.x + queen.y) == (queen_to_check.x + queen_to_check.y)) #check right diagonale
        end
      end
    end
    attacks/2 unless attacks == 0
  end


  #move with one queen, in any direction. In case of falling out of board, start from the beginning of that row/column
  def propose_solution(queens)
    #randomly pick one queen and make a move with her
    random_position = Random.rand(0...SIZE)
    queen_to_move = queens[random_position]
    processed = 1
    new_x = 0
    new_y = 0
    i = 0

    while processed != SIZE

      if i == SIZE
        processed = 1
        i = 0
      end

      #move the queen randomly
      new_x = Random.rand(0...SIZE) + queen_to_move.x
      new_y = Random.rand(0...SIZE) + queen_to_move.y

      new_x = (new_x - (SIZE-1)) if new_x >= (SIZE)
      new_y = (new_y - (SIZE-1)) if new_y >= (SIZE)

      while i < SIZE
        act_queen = queens[i]

        unless queen_to_move == act_queen
          unless ((new_x == act_queen.x) && (new_y == act_queen.y))
            processed += 1
          end
        end
        i+=1
      end
    end

    #puts "Old positions #{queen_to_move.x} and #{queen_to_move.y}"
    #puts "New positions #{new_x} and #{new_y}"

    new_queens = save_new_positions(random_position, queens)
    new_queens[random_position] = Queen.new(new_x, new_y)

    new_queens
  end

  def propose_solution_2(queens)
    random_position1 = rand(0...SIZE)
    random_position2 = rand(0...SIZE)

    until random_position1 != random_position2
      random_position1 = rand(0...SIZE)
      random_position2 = rand(0...SIZE)
    end

    new_queens = Array.new

    for i in 0...SIZE
      new_queens[i] = queens[i].copy_yourself
    end

    temp_x = new_queens[random_position1].x
    new_queens[random_position1].x = queens[random_position2].x
    new_queens[random_position2].x = temp_x

    new_queens
  end

  def save_new_positions(random_position, queens)
    new_queens = Array.new

    for i in 0...SIZE
      new_queens[i] = queens[i].copy_yourself unless i == random_position
    end
    new_queens
  end

  def accept_new_solution(queens)
    save_new_positions(SIZE, queens)
  end

  def acceptance_probability(new_attacks, attacks,temperature)
    return true if(new_attacks < attacks) #new solution is better, accept
    return true if(Math.exp(-(new_attacks-attacks)/temperature) > rand()) #accept new solution with probability
    return false #refuse
  end

end
SIZE = 8

class Main_Util

  def initialize_queens
    #initialize queens, to every row insert queen into random column
    taken_columns = Hash.new
    queens = Array.new

    for i in (0...SIZE)
      column = rand(0...SIZE)

      until(taken_columns.has_key?(column))
        column = rand(0...SIZE)
        taken_columns[column] = true
      end

      queens[i] = column
    end

    return queens
  end

  def print_board(queens)
    for i in (0...SIZE)
      print "\n" if i > 0
      for j in (0...SIZE)
        if(queens[i] == j)
          print '1 '
        else
          print '0 '
        end
      end
    end
    print "\n"
  end

  def calculate_attacks(queens)
    attacks = 0

    queens.each_with_index do |queen, index|
      queen_x = index
      queen_y = queen

      for i in (0...SIZE)
        c_queen_x = i
        c_queen_y = queens[i]

        unless queen_x == c_queen_x #omit yourself - when using different solutions
          attacks += 1 if queen_x == c_queen_x #check rows for queens
          attacks += 1 if queen_y == c_queen_y #check columns for queens
          attacks += 1 if ((queen_y - queen_x) == (c_queen_y - c_queen_x)) #check left diagonal
          attacks += 1 if ((queen_x + queen_y) == (c_queen_x + c_queen_y)) #check right diagonal
        end
      end
    end
    attacks/2 unless attacks == 0
  end

  def find_neighbour(queens)
    position_1 = rand(0...SIZE)
    position_2 = rand(0...SIZE)

    unless position_1 != position_2
      position_1 = rand(0...SIZE)
      position_2 = rand(0...SIZE)
    end

    temp_1 = position_1
    queens[position_1] = position_2
    queens[position_2] = temp_1

    queens
  end

  def find_neighbour_2(queens)
    position_1 = rand(0...SIZE)
    position_2 = rand(0...SIZE)

    unless position_1 != position_2
      position_1 = rand(0...SIZE)
      position_2 = rand(0...SIZE)
    end

    queens[position_1] = position_2

    queens
  end

  def acceptance_probability(new_attacks, attacks,temperature, queens)
    begin
      return true if(new_attacks < attacks) #new solution is better, accept
      return true if(Math.exp(-(new_attacks-attacks)/temperature) > rand()) #accept new solution with probability
      return false #refuse

    rescue
      puts new_attacks
      puts attacks
    end
  end
end
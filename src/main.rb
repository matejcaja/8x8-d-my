require_relative("main_util")

ALPHA = 0.85
PROGRAM_ITERATIONS = 100
MAX_ITERATIONS = 200
TEMP = 100.0

for i in (0...PROGRAM_ITERATIONS)
  temp = TEMP
  util = Main_Util.new

  queens = util.initialize_queens
  attacks = util.calculate_attacks(queens)
  total_iterations = 0

  for i in (0...MAX_ITERATIONS)
    new_queens = util.find_neighbour_2(Array.new(queens))
    new_attacks = util.calculate_attacks(new_queens)

    temp *= ALPHA

    if util.acceptance_probability(new_attacks, attacks, temp, new_queens)
      queens = new_queens
      attacks = new_attacks

      util.print_board queens
      puts "Attacks: #{attacks}"
      print "\n\n"
    end



    if attacks == 0
      total_iterations = i
      #puts total_iterations
      break
    end

  end

  #puts "End of algorithm: "
  util.print_board queens
  puts "Attacks: #{attacks}"
end




